package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UnitDao {
    List<Unit> getUnitList(Integer rows);
}
