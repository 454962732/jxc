package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description 商品类别
 */
@Service
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId( Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    List<Goods> getGoodsByTypeId(Integer goodsTypeId);


    GoodsType getGoodsTypeById(Integer goodsTypeId);

    Integer saveGoodsType( GoodsType goodsType);


    Integer delete(Integer goodsTypeId);
}
