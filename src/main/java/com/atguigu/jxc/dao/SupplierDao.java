package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SupplierDao {


    List<Supplier> getSupplierList(@Param("offset") int offSet, @Param("pageRow") Integer rows,@Param("supplierName") String supplierName);

    Object getSupplierCount(@Param("supplierName") String supplierName);

    void updateSupplier(Supplier supplier);

    void saveSupplier(Supplier supplier);

    void deleteSupplier(int parseInt);
}
