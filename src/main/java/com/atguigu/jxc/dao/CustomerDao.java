package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerDao {

    List<Customer> getcustomerList(@Param("offSet") int offSet, @Param("pageRow") Integer rows, @Param("customerName") String customerName);

    Object getCustomerCount(@Param("customerName") String customerName);

    void saveCustomer( Customer customer);

    void updateCustomer( Customer customer);

    void deleteCustomer( int parseInt);
}
