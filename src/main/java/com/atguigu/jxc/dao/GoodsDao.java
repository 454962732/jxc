package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @description 商品信息
 */

@Service
public interface GoodsDao {


    String getMaxCode();


    

    Object getGoodsInventoryCount(@Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    Goods findByGoodsId(Integer goodsId);

    Integer updateGoods(Goods goods);

    List<Goods> getGoodsInventoryList(@Param("offSet") Integer offSet,@Param("pageRow") Integer pageRow, @Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoodsList(@Param("offSet") int offSet, @Param("pageRow")Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    Object getGoodCount(String goodsName);

    Integer saveGoods(Goods goods);

    Integer deleteGoods(Goods goods);

    List<Goods> getNoInventoryQuantityList(@Param("offSet") Integer offSet,@Param("pageRow") Integer pageRow,@Param("nameOrCode") String nameOrCode);

    Object getNoInventoryQuantityCount(@Param("nameOrCode")String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("offSet") Integer offSet,@Param("pageRow") Integer pageRow,@Param("nameOrCode") String nameOrCode);

    Object getHasInventoryQuantityCount(@Param("nameOrCode")String nameOrCode);

    void saveStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") double purchasingPrice);

    List<Goods> listAlarm();
}
