package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DamageListGoodsDao {
    

    void saveDamageList(DamageList damageList);

    void saveDamageListGoods(DamageListGoods damageListGoods);


    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<DamageListGoods> goodsList(Integer damageListId);
}
