package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OverflowListGoodsDao {


    void saveOverflowList(OverflowList overflowList);

    void saveOverflowListGoodsList(OverflowListGoods overflowListGoods);

    List<OverflowList> list(@Param("sTime") String sTime,@Param("eTime") String eTime);

    List<OverflowListGoods> goodsList(Integer overflowListId);
}
