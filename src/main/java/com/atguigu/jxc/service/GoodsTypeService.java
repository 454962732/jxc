package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

/**
 * @description
 */
public interface GoodsTypeService {
    String loadGoodsType();

    ServiceVO save(String goodsTypeName, Integer pId);

    ServiceVO delete(Integer goodsTypeId);

}
