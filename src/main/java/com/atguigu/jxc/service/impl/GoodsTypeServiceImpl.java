package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {


    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public String loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));

        return this.getAllGoodsType(-1).toString(); // 根节点默认从-1开始
    }


    @Override
    public ServiceVO save(String goodsTypeName, Integer pId) {

        GoodsType goodsType = new GoodsType(goodsTypeName , 0 ,pId) ;
        goodsTypeDao.saveGoodsType(goodsType) ;

        GoodsType parentGoodsType = goodsTypeDao.getGoodsTypeById(pId);

        if(parentGoodsType.getGoodsTypeState() == 0) {

            parentGoodsType.setGoodsTypeState(1);

            goodsTypeDao.updateGoodsTypeState(parentGoodsType);

        }

        return new  ServiceVO(SuccessCode.SUCCESS_CODE ,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(Integer goodsTypeId) {
        List<Goods> goodsList =goodsTypeDao.getGoodsByTypeId(goodsTypeId) ;

        if (goodsList.size() != 0){
            return new ServiceVO<>(ErrorCode.GOODS_TYPE_ERROR_CODE ,ErrorCode.GOODS_TYPE_ERROR_MESS) ;
        }
        GoodsType goodsType = goodsTypeDao.getGoodsTypeById(goodsTypeId) ;


        List<GoodsType> goodsTypeList =goodsTypeDao.getAllGoodsTypeByParentId(goodsType.getPId()) ;

        if (goodsList.size() == 1){
            GoodsType parentGoodstype =goodsTypeDao.getGoodsTypeById(goodsType.getPId()) ;
            parentGoodstype.setGoodsTypeState(0);
            goodsTypeDao.updateGoodsTypeState(parentGoodstype) ;
        }
        goodsTypeDao.delete(goodsTypeId) ;


        return new ServiceVO(SuccessCode.SUCCESS_CODE ,SuccessCode.SUCCESS_MESS) ;
    }

    /**
     * 递归查询所有商品类别
     * @return
     */
    public JsonArray getAllGoodsType(Integer parentId){

        JsonArray array = this.getGoodSTypeByParentId(parentId);

        for(int i = 0;i < array.size();i++){

            JsonObject obj = (JsonObject) array.get(i);

            if(obj.get("state").getAsString().equals("open")){// 如果是叶子节点，不再递归

                continue;

            }else{// 如果是根节点，继续递归查询

                obj.add("children", this.getAllGoodsType(obj.get("id").getAsInt()));

            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     * @return
     */
    public JsonArray getGoodSTypeByParentId(Integer parentId){

        JsonArray array = new JsonArray();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        //遍历商品类别
        for(GoodsType goodsType : goodsTypeList){

            JsonObject obj = new JsonObject();

            obj.addProperty("id", goodsType.getGoodsTypeId());//ID

            obj.addProperty("text", goodsType.getGoodsTypeName());//类别名称

            if(goodsType.getGoodsTypeState() == 1){

                obj.addProperty("state", "closed"); //根节点

            }else{

                obj.addProperty("state", "open");//叶子节点

            }

            obj.addProperty("iconCls", "goods-type");//图标默认为自定义的商品类别图标

            JsonObject attributes = new JsonObject(); //扩展属性

            attributes.addProperty("state", goodsType.getGoodsTypeState());//就加入当前节点的类型

            obj.add("attributes", attributes);

            array.add(obj);

        }

        return array;
    }



}
