package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;


    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {

        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Supplier> suppliers = supplierDao.getSupplierList(offSet, rows, supplierName);

        map.put("rows", suppliers);
        map.put("total", supplierDao.getSupplierCount(supplierName));
        return map;

    }

    @Override
    public ServiceVO save(Supplier supplier) {

        if (supplier.getSupplierId() == null) {
            supplierDao.saveSupplier(supplier);

        } else {
            supplierDao.updateSupplier(supplier);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {
        String[] idArray = ids.split(",");

        for (String id : idArray) {

            supplierDao.deleteSupplier(Integer.parseInt(id));

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);


    }
}
