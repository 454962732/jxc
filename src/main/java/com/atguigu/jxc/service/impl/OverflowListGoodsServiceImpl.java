package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private UserDao userDao ;

    @Autowired
    private GoodsDao goodsDao ;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao ;


    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {
        Gson gson =new Gson() ;
        List<OverflowListGoods> overflowListGoodsList =gson.fromJson(overflowListGoodsStr ,new TypeToken<List<OverflowListGoods>>(){}.getType()) ;

        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        overflowList.setUserId(currentUser.getUserId());

        overflowListGoodsDao.saveOverflowList(overflowList);

        for (OverflowListGoods overflowListGoods : overflowListGoodsList ){
            overflowListGoods.setOverflowListGoodsId(overflowList.getOverflowListId());
            overflowListGoodsDao.saveOverflowListGoodsList(overflowListGoods);

            Goods goods = goodsDao.findByGoodsId(overflowListGoods.getGoodsId());

            goods.setInventoryQuantity(goods.getInventoryQuantity()+overflowListGoods.getGoodsNum());

            goods.setState(2);

            goodsDao.updateGoods(goods);
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE ,SuccessCode.SUCCESS_MESS) ;
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String ,Object>map =new HashMap<>() ;
        try {
            List<OverflowList>overflowListList = overflowListGoodsDao.list(sTime ,eTime) ;
            map.put("row" ,overflowListList) ;
        }catch (Exception e) {

            e.printStackTrace();

        }


        return map ;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String ,Object>map =new HashMap<>() ;
        try {
            List<OverflowListGoods>overflowListGoodsList = overflowListGoodsDao.goodsList(overflowListId) ;
            map.put("rows" ,overflowListGoodsList) ;
        }catch (Exception e) {

            e.printStackTrace();

        }

        return map ;
    }


}
