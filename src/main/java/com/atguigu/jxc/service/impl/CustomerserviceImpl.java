package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.Customerservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerserviceImpl implements Customerservice {

    @Autowired
    private CustomerDao customerDao ;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String , Object> map =new HashMap<>() ;
        page = page == 0 ? 1 :page ;
        int offSet = (page -1) *rows ;
        List<Customer> customers =customerDao.getcustomerList(offSet , rows , customerName);

        map.put("rows" ,customers);
        map.put("total" ,customerDao.getCustomerCount(customerName)) ;

        return map ;
    }

    @Override
    public ServiceVO save(Customer customer) {
        if (customer.getCustomerId() == null){
            customerDao.saveCustomer(customer) ;
        }else {
            customerDao.updateCustomer(customer);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE ,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(String ids) {
        String[] idArray = ids.split(",");

        for(String id :idArray) {
            customerDao.deleteCustomer(Integer.parseInt(id));
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE ,SuccessCode.SUCCESS_MESS);
    }
}
