package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private GoodsDao goodsDao ;

    @Autowired
    private UserDao userDao ;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao ;

    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        Gson gson =new Gson() ;

        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr,new TypeToken<List<DamageListGoods>>(){}.getType());

        User currentUser = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());

        damageList.setUserId(currentUser.getUserId());

        damageListGoodsDao.saveDamageList(damageList) ;


        for(DamageListGoods damageListGoods : damageListGoodsList){

            damageListGoods.setDamageListId(damageList.getDamageListId());

            damageListGoodsDao.saveDamageListGoods(damageListGoods);

            // 修改商品库存，状态
            Goods goods = goodsDao.findByGoodsId(damageListGoods.getGoodsId());

            goods.setInventoryQuantity(goods.getInventoryQuantity()-damageListGoods.getGoodsNum());

            goods.setState(2);

            goodsDao.updateGoods(goods);

        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE ,SuccessCode.SUCCESS_MESS) ;

    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String ,Object> map =new HashMap<>() ;

     try {
         List<DamageList>damageListList =damageListGoodsDao.list(sTime ,eTime) ;
         map.put("rows" ,damageListList) ;
     }catch (Exception e){
         e.printStackTrace();
     }
             return map ;
         }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String ,Object> map =new HashMap<>() ;
        List<DamageListGoods>damageListGoodsList = damageListGoodsDao.goodsList(damageListId) ;

        map.put("rows" , damageListGoodsList) ;

        return map;
    }
}
