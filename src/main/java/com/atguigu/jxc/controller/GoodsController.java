package com.atguigu.jxc.controller;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;


@RestController
@RequestMapping(value = "/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;


    @RequestMapping(value = "listInventory")
    public Map<String,Object> listInventor( Integer page, Integer rows , String codeOrName, Integer goodsTypeId){


        return goodsService.listInventory(page, rows, codeOrName, goodsTypeId);

    }




    @PostMapping(value = "/list")
    public Map<String ,Object>list(Integer page, Integer rows, String goodsName, Integer goodsTypeId){
        return goodsService.list(page ,rows ,goodsName ,goodsTypeId);

    }



    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }



    @PostMapping(value = "/save")
    @ResponseBody
    public ServiceVO save (Goods goods){
        return goodsService.save(goods) ;
    }



    @PostMapping(value = "/delete")
    @ResponseBody
    public ServiceVO delete(Goods goods){
        return goodsService.delete(goods);
    }

@PostMapping(value = "/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity( Integer page,Integer rows,String nameOrCode){
    return goodsService.getNoInventoryQuantity(page ,rows ,nameOrCode) ;

    }




    @PostMapping(value = "/getHasInventoryQuantity")
    public Map<String,Object>getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        return goodsService.getHasInventoryQuantity(page ,rows ,nameOrCode) ;
    }



    @PostMapping(value = "/saveStock")
    public ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice){
        return goodsService.saveStock(goodsId ,inventoryQuantity ,purchasingPrice) ;


    }


    @PostMapping(value = "/deleteStock")
    public ServiceVO deleteStock(Integer goodsId){
        return goodsService.deleteStock(goodsId) ;

    }


    @PostMapping(value = "/listAlarm")
    public Map<String,Object> listAlarm(){
        return goodsService.listAlarm() ;

    }

}
